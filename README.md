# DeSeMod (Détection Sémantique des Modaux)
## Description

DeSeMod est un projet visant à analyser l'utilisation des verbes modaux français "vouloir", "falloir", "pouvoir" et "devoir" dans le corpus "CoLATeP". L'objectif principal du projet est de déterminer les risques d'ambiguïté ou d'imprécision dans l'utilisation de ces verbes et dans quels contextes et avec quelle signification ils peuvent être utilisés de manière non ambiguë.

## Objectifs

* Analyser le corpus "CoLATeP" pour identifier les occurrences des verbes modaux mentionnés.
* Catégoriser et cataloguer les différents contextes d'utilisation de ces verbes.
* Déterminer quand leur utilisation est ambiguë et quand elle est précise en fonction du contexte.
* Fournir des informations et une analyse détaillée sur l'utilisation de ces verbes.

## Structure du Projet

    data/: Répertoire contenant les données du corpus "CoLATeP" séléctionnées pour notre étude.
    data_selection/: Répertoire contenant les sripts permetant la sélection de données du corpus "CoLATeP" pour notre étude.
    scripts/: Répertoire contenant les scripts et le code pour l'analyse des données.

## Auteurs

* DOBRESCU, Anca
* MENDOCA MANHOSO, Angelo
* MONTENEGRO, Génesis

