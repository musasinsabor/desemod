import re
from lxml import etree
import glob
import os
import argparse
import pandas as pd


def extract_category_from_file(xml_file):
    try:
        # Parse the XML file
        tree = etree.parse(xml_file)

        # Find the <div> tag with type="mainCategory"
        category_divs = tree.find(".//div[@type='mainCategory']")
        for p in category_divs:
            category_p = p.text
        return category_p
    except Exception as e:
        #print("Error processing the XML file:", e)
        return None


def process_all_xml_files_in_directory(directory):
    names = []
    categories = []
    current_directory = os.getcwd()  # Get the current directory
    xml_directory = os.path.join(current_directory, directory)
    xml_files = glob.glob(f"{xml_directory}/*.xml")

    if not xml_files:
        print(f"No XML files found in the directory: {current_directory}")
        return
    for xml_file in xml_files:
        category = extract_category_from_file(xml_file)
        xml_name = re.search(r"(.*)\/(.*).xml", xml_file)
        if category:
            # print(f"Content of '{xml_file}':")
            names.append(xml_name.group(2))
            categories.append(category)
            # print("\n")
        else:
            #print(f"No 'category' found in '{xml_file}'.")
            names.append(xml_name.group(2))
            categories.append("iFixit")
    return names, categories


if __name__ == "__main__":
    # args parser config
    parser = argparse.ArgumentParser(description="Process XML files in a directory.")
    parser.add_argument("directory", help="Path to the directory containing XML files")

    # args parser

    args = parser.parse_args()
    # Function usage
    categories = process_all_xml_files_in_directory(args.directory)
    data = {
        'Files': categories[0],
        'Categories': categories[1]
    }
    df = pd.DataFrame(data)
    df.to_csv('data/category_selection.csv', index=False)
    set(categories[1])
