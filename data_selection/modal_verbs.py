import fr_core_news_md
import parser
import argparse


def verb_verification(tokens):
    verbs = ["pouvoir", "devoir", "falloir", "vouloir"]
    verb_counts = {verb: 0 for verb in verbs}
    verb_found = False
    for token in tokens:
        for verb in verbs:
            if token.lemma_ == verb and token.pos_ == "VERB":
                verb_counts[verb] += 1
                verb_found = True

    return verb_counts, verb_found



def analyse(texts, nlp):
    filtered_texts = []
    filtered_verbs = []
    filtered_names = []
    filtered_dir = []
    pouvoir = []
    devoir = []
    vouloir = []
    falloir = []
    for i in range(0, 400):
        doc = nlp(texts[1][i])
        counts, found = verb_verification(doc)
        if found:
            filtered_texts.append(texts[1][i])
            filtered_names.append(texts[0][i])
            filtered_verbs.append(counts)
            pouvoir.append(counts["pouvoir"])
            devoir.append(counts["devoir"])
            vouloir.append(counts["vouloir"])
            falloir.append(counts["falloir"])
        else:
            pass
    return filtered_names, filtered_texts, filtered_verbs, pouvoir, devoir, vouloir, falloir



if __name__ == "__main__":
    # args parser config
    xparser = argparse.ArgumentParser(description="Process XML files in a directory.")
    xparser.add_argument("directory", help="Path to the directory containing XML files")
    #xparser.add_argument("filter", help="String with the lemmas to search")

    # args parser
    args = xparser.parse_args()
    # Function usage

    instructions = parser.process_all_xml_files_in_current_directory(args.directory)
    print(f"instructions extracted from {args.directory}")
    nlp = fr_core_news_md.load()  # download french model
    analyse(instructions, nlp)
    print("data filtered")

