import modal_verbs
import argparse
import parser
import pandas as pd
import re
import glob
import category_selection

if __name__ == "__main__":
    # args parser config
    xparser = argparse.ArgumentParser(description="Process XML files in a directory.")
    xparser.add_argument("directory", help="Path to the directory containing XML files")
    #xparser.add_argument("filter", help="String with the lemmas to search")
    # args parser
    args = xparser.parse_args()
    all_categories = category_selection.process_all_xml_files_in_directory("./data/all/")
    target_categories = set(all_categories[1])
    dirs = glob.glob(f"{args.directory}/*")
    all_data = []
    for path in dirs:
        for category in target_categories:
            if re.search(rf"{category}", path):
                instructions = parser.process_all_xml_files_in_current_directory(path)
                print(f"instructions extracted from {path}")
                filtered_data = modal_verbs.analyse(instructions)
                print(f"data filtered from {path}")
                dir_name = re.search(r"(.*)\/(.*)", path)
                # Build a DataFrame
                data = {
                    'Texts': filtered_data[0],
                    'Verbs': filtered_data[1],
                    'Pouvoir': filtered_data[2],
                    'Devoir': filtered_data[3],
                    'Vouloir': filtered_data[4],
                    'Falloir': filtered_data[5]
                }
                df = pd.DataFrame(data)
                all_data.append(df)
                print(f"{dir_name.group(2)} DataFrame saved")
    # Save the DataFrame como as CSV file
    combined_df = pd.concat(all_data, ignore_index=True)
    combined_df.to_csv('data/combined_data.csv', index=False)
    print("DataFrame saved")


