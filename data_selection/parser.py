import re
from lxml import etree
import glob
import os
import argparse


def extract_text_from_instruction(xml_file):
    try:
        # Parse the XML file
        tree = etree.parse(xml_file)

        # Find the <div> tag with type="instruction"
        instruction_divs = tree.findall(".//div[@type='instruction']")
        warnings_divs = tree.findall(".//div[@type='warnings']")
        combined_divs = instruction_divs + warnings_divs
        instructions = []

        if combined_divs:
            # Iterate through instruction_divs
            for div in combined_divs:
                # Use itertext to extract text from the div and its children
                div_text = " ".join(text for text in div.itertext() if text.strip())
                instructions.append(div_text)
            instructions = "\n".join(instructions)
            return instructions
        else:
            return None
    except Exception as e:
        print("Error processing the XML file:", e)
        return None



def process_all_xml_files_in_current_directory(directory):
    names = []
    texts = []
    current_directory = os.getcwd()  # Get the current directory
    subdirectory = directory  # Subdir additionals
    xml_directory = os.path.join(current_directory, subdirectory)
    xml_files = glob.glob(f"{xml_directory}/*.xml")

    if not xml_files:
        print(f"No XML files found in the directory actual: {current_directory}")
        return
    for xml_file in xml_files:
        text_from_instruction = extract_text_from_instruction(xml_file)
        xml_name = re.search(r"(.*)\/(.*).xml", xml_file)
        if text_from_instruction:
            # print(f"Content of '{xml_file}':")
            names.append(xml_name.group(2))
            texts.append(text_from_instruction)
                # print(f"Text {idx + 1}: {text}")
            # print("\n")
        else:
            print(f"No 'instruction' found in '{xml_file}'.")
    return names, texts


if __name__ == "__main__":
    # args parser config
    parser = argparse.ArgumentParser(description="Process XML files in a directory.")
    parser.add_argument("directory", help="Path to the directory containing XML files")

    # args parser
    args = parser.parse_args()
    # Function usage
    data = process_all_xml_files_in_current_directory(args.directory)

