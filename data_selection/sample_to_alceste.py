import argparse
import parser
import re
import glob
import pandas as pd
import modal_verbs
import fr_core_news_md
import category_selection

# change here the target categories to extract

if __name__ == "__main__":
    # args parser config
    xparser = argparse.ArgumentParser(
        description="Process XML files in a directory and create Alceste corpus file."
    )
    xparser.add_argument("directory", help="Path to the directory containing XML files")
    # xparser.add_argument("filter", help="String with the lemmas to search")
    # args parser
    args = xparser.parse_args()
    dirs = glob.glob(f"{args.directory}/*")
    all_categories = category_selection.process_all_xml_files_in_directory("./data/all/")
    target_categories = set(all_categories[1])
    # output name definition
    output_filename = "corpus_instructions_risk"
    all_files = []
    all_instructions = []
    all_categories = []
    all_verbs = []
    print("starting")
    nlp = fr_core_news_md.load()  # download french model
    print("model loaded")
    # create the dataframe with the instructions
    for path in dirs:
        for category in target_categories:
            if re.search(rf"{category}", path):
                print(f"reading {path}")
                instructions = parser.process_all_xml_files_in_current_directory(path)
                modals = modal_verbs.analyse(instructions, nlp)
                print(f"Instructions extracted from {path}")
                all_files.extend(modals[0][:700])
                all_instructions.extend(modals[1][:700])
                all_verbs.extend(modals[2][:700])
                for _ in range(0, len(modals[1][:700])):
                    all_categories.append(category)

    print(len(all_categories), len(all_files), len(all_instructions))
    data = {
        "Category": all_categories,
        "File": all_files,
        "Instructions": all_instructions,
        "Verbs": all_verbs
    }
    df = pd.DataFrame(data)

    # Save the DataFrame como as CSV file
    df.to_csv(f"{output_filename}.csv", index=False)
    print(f"All data saved in {output_filename}.csv")

    with open(output_filename + ".txt", "w", encoding="utf-8") as output_file:
        for i in range(0, len(all_files)):
            output_file.write(
                "**** "
                + f"*category_{all_categories[i]}"
                + " "
                + f"*file_{all_files[i]}\n"
            )
            output_file.write(all_instructions[i])
            output_file.write("\n\n")  # Add a empty line

        print(f"Data saved in {output_filename}.txt")