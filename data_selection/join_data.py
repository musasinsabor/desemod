import pandas as pd
import os
import glob
import argparse

def join_data(directory_path):
    # Get the list of CSV files in the directory
    print(directory_path)
    csv_files = glob.glob(os.path.join(directory_path, "*.csv"))

    # Check if CSV files were found
    if not csv_files:
        print("No CSV files found in the directory.")
    else:
        # Create a list to store DataFrames from each CSV file
        df_list = []

        # Read and combine the CSV files into a single DataFrame
        for csv_file in csv_files:
            df = pd.read_csv(csv_file)
            df_list.append(df)

        # Combine the DataFrames into a single one
        combined_df = pd.concat(df_list, ignore_index=True)

        # Save the combined DataFrame to a new CSV file
        combined_csv_file = f"combined_data.csv"
        combined_df.to_csv(combined_csv_file, index=False)
        print(f"Combined CSV file has been created at: {combined_csv_file}")

if __name__ == "__main__":
    xparser = argparse.ArgumentParser(description="Join CVS files in a directory.")
    xparser.add_argument("directory", help="Path to the directory containing CSV files")
    args = xparser.parse_args()
    join_data(args.directory)

