import pandas as pd
import stanza
import argparse
import re

try:
    import nltk

    nltk.download("punkt")
    from nltk.tokenize import sent_tokenize
except:
    print("Try installing firts with", "!python3 -m pip install", sep="\n\n")


def extract_sentences_with_lemmas(df):
    nlp = stanza.Pipeline(
        "fr", processors="tokenize,pos,lemma,depparse", tokenize_pretokenized=True
    )
    files = []
    categories = []
    full_instructions = []
    sentence_type = []
    dependencies = []
    sentences = []

    for idx in range(600, 600 + len(df["Instructions"])):
        instruction = df["Instructions"][idx]
        lemmas = {"pouvoir", "devoir", "vouloir"}  # Use a set for more efficient lookup

        for i, instr in enumerate(sent_tokenize(instruction)):
            # Process the text
            doc = nlp(instr)

            # Extract dependency analysis for lemmas of interest
            presence = False
            for sentence in doc.sentences:
                sentence_dependencies = []
                for word in sentence.words:
                    dependency_info = {
                        "word": word.text,
                        "lemma": word.lemma,
                        "POS": word.pos,
                        "head": word.head,
                        "dependency_relation": word.deprel,
                        "features": word.feats,
                    }
                    sentence_dependencies.append(dependency_info)
                    if word.lemma in lemmas:
                        presence = True

            # Create sentences containing lemma-related words
            if presence:
                if i > 0 and i + 1 < len(doc.sentences):
                    sentences.append(
                        doc.sentences[i - 1].text
                        + " "
                        + sentence.text
                        + " "
                        + doc.sentences[i + 1]
                    )
                elif i > 0 and i + 1 < len(doc.sentences):
                    sentences.append(sentence.text + " " + doc.sentences[i + 1])
                else:
                    sentences.append(sentence.text)

                # Verification: instruction or description?
                for dep in sentence_dependencies[
                    :2
                ]:  # Check first 2 elements for Infinitive and Imperative
                    if dep.get("features") is not None and re.search(
                        r"VerbForm=Inf|Mood=Imp", dep.get("features")
                    ):
                        presence = True
                        # sentence_type.append(True)
                    else:
                        presence = False
                dependencies.append(sentence_dependencies)
                sentence_type.append(presence)
                files.append(df["File"][idx])
                categories.append(df["Category"][idx])
                full_instructions.append(df["Instructions"][idx])

    data = {
        "File": files,
        "Category": categories,
        "Instructions": full_instructions,
        "SentenceType": sentence_type,
        "Dependencies": dependencies,
        "Sentences": sentences,
    }

    return data


if __name__ == "__main__":
    # args parser config
    xparser = argparse.ArgumentParser(
        description="Process XML files in a directory and create Alceste corpus file."
    )
    xparser.add_argument("corpus_path", help="Path to the corpus file, as a .csv file")
    # args parser
    args = xparser.parse_args()
    df = pd.read_csv(args.corpus_path)
    print("Data read")
    data = extract_sentences_with_lemmas(df)
    print("Your analyse is ready")
    new_df = pd.DataFrame(data)
    new_df.to_csv(f"{args.corpus_path[:-4]}_analysed.csv")
    print(f"Your data is saved as ", f"{args.corpus_path[:-4]}_analysed.csv")
